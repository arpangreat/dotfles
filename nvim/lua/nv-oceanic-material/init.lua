vim.api.nvim_exec([[
set background=dark
colorscheme oceanic_material
highlight Normal guibg=none
let g:oceanic_material_transparent_background = 1
let g:oceanic_material_allow_bold = 1
let g:oceanic_material_allow_italic = 1
let g:oceanic_material_allow_underline = 1
let g:oceanic_material_allow_undercurl = 1
let g:oceanic_material_allow_reverse = 1
]], true)
