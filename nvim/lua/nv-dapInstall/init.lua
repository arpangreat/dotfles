local dap_install = require("dap-install")

dap_install.setup({
	installation_path = "/tmp/test_dap_install/",
	verbosely_call_debuggers = true,
})
