vim.api.nvim_exec([[
colorscheme gruvbox-flat
highlight Normal guibg=none
]], true)
vim.g.gruvbox_flat_style = "dark"
vim.g.gruvbox_italic_functions = true
vim.g.gruvbox_italic_variables = true
